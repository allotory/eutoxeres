# Eutoxeres IM (Deprecated)

## What is Eutoxeres IM?

Eutoxeres IM ("Eutoxeres Instant Messenger") is an instant messenger for the desktop.

## Snapshot

* Swing version

![screenshot1](./snapshot/swinglogin.PNG)

![screenshot2](./snapshot/swingmain.PNG)

![screenshot3](./snapshot/swingchat.PNG)

* Web version(Electron)

![screenshot4](./snapshot/electronlogin.PNG)

![screenshot5](./snapshot/electronmain.PNG)

![screenshot6](./snapshot/electronchat.PNG)

## Contributing

Pull requests are being accepted! If you would like to contribute, simply fork
the project and make your changes.

##Support:

Support now is given by me.

## License

The MIT License